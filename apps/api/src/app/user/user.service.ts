import { ForbiddenException, Injectable } from '@nestjs/common';
import { LoginDTO, User, UserDTO } from '@dar-academy/domain';

@Injectable()
export class UserService {
  public authUser(creds: LoginDTO): UserDTO {
    const user = mockData.find(
      (u) => u.username === creds.username && u.password === creds.password
    );
    if (!user) {
      throw new ForbiddenException('Wrong creds');
    }

    return {
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
    };
  }
}

const mockData: User[] = [
  {
    firstName: 'Miras',
    lastName: 'Magzom',
    username: 'mmagzom',
    password: '1234',
  },
];
