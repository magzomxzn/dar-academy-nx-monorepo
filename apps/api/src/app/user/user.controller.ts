import { LoginDTO } from '@dar-academy/domain';
import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('login')
  login(@Body() body: LoginDTO) {
    return this.userService.authUser(body);
  }
}
