import { Provider } from 'react-redux';
import LoginForm from './components/login/LoginForm';
import store from './redux/store';

export function App() {
  return (
    <Provider store={store}>
      <LoginForm />
      <div />
    </Provider>
  );
}

export default App;
