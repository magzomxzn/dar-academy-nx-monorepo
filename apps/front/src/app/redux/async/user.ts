import { LoginDTO } from '@dar-academy/domain';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { login } from '../../services/user';

export const loginRequest = createAsyncThunk(
  'user/loginRequest',
  async (data: LoginDTO) => {
    const response = await login(data);
    return response;
  }
);
