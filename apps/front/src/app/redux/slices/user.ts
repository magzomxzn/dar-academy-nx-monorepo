import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LoginDTO, User } from '@dar-academy/domain';
import { loginRequest } from '../async/user';

export interface UserState {
  loginError: string;
  user: User | null;
  loginInProgress: boolean;
}

const initialState: UserState = {
  loginError: '',
  user: null,
  loginInProgress: false,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    getUser: (state, payload: PayloadAction<LoginDTO>) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      //   state.value += 1
      // state.loginError = 'Error'
      console.log(payload);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder
      .addCase(loginRequest.pending, (state, action) => {
        state.loginInProgress = true;
        state.user = null;
        state.loginError = '';
      })
      .addCase(loginRequest.fulfilled, (state, action: PayloadAction<User>) => {
        state.user = action.payload;
        state.loginInProgress = false;
      })
      .addCase(loginRequest.rejected, (state, action) => {
        state.loginError = `${action.error.name} ${action.error.message}`;
        state.loginInProgress = false;
      });
  },
});

// Action creators are generated for each case reducer function
export const { getUser } = userSlice.actions;

export default userSlice.reducer;
