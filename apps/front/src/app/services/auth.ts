import {
  AuthTokens,
  Profile,
  ProfileUpdateDTO,
  RegisterResponse,
  SendOtpResponse,
  SignUpResponse,
} from '@dar-academy/domain';

import axios, {
  AxiosResponse,
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
} from 'axios';

import { EventEmitter } from 'events';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

export enum AuthStatus {
  UNAUTHORIZED = 'UNAUTHORIZED',
}

export interface AuthConfig {
  griffonApiRoot: string;
  griffonClientId: string;
  griffonClientSecret: string;
}

export const TOKEN_STORAGE_KEY = '5q-web-auth';
export const PROFILE_STORAGE_KEY = '5q-web-profile';

export class AuthService {
  httpClient: AxiosInstance;

  private tokenRefreshing: Promise<void> | null = null;

  public loginStatus = new EventEmitter();

  public loggedIn = new BehaviorSubject(null);

  constructor(private config: AuthConfig) {
    this.httpClient = axios.create();

    this.httpClient.interceptors.response.use(
      (response: AxiosResponse) => response,
      this.refreshTokenErrorInterceptor
    );
  }

  static createInstance(config: AuthConfig) {
    return new AuthService(config);
  }

  public createHttpClient(axiosConfig?: AxiosRequestConfig): AxiosInstance {
    const httpClient = axios.create(axiosConfig);
    httpClient.interceptors.request.use(this.authTokenRequestInterceptor);
    return httpClient;
  }

  private authTokenRequestInterceptor = async (config: AxiosRequestConfig) => {
    // Don't override defined header
    if (config.headers?.['Authorization']) {
      return config;
    }
    if (this.tokenRefreshing) {
      await this.tokenRefreshing;
    }
    const tokens = this.getTokens();
    const now = new Date();
    // Token is expired
    if (
      !tokens ||
      (tokens.expire_date && tokens.expire_date - now.getTime() <= 0)
    ) {
      this.tokenRefreshing = new Promise((resolve) => {
        this.refreshToken()
          .then((newTokens) => {
            this.persistTokens(newTokens);
          })
          .catch(() => {
            this.loginStatus.emit(AuthStatus.UNAUTHORIZED, true);
          })
          .finally(() => {
            resolve();
            this.tokenRefreshing = null;
          });
      });
    }
    // Add token to headers
    const idToken = this.getIdToken();
    if (idToken) {
      config.headers = {
        ...config.headers,
        Authorization: `Bearer ${idToken}`,
      };
    } else {
      delete config.headers?.['Authorization'];
    }
    return config;
  };

  public getIdToken = (): string | null => {
    let tokens: AuthTokens | null = null;
    try {
      tokens = JSON.parse(localStorage.getItem(TOKEN_STORAGE_KEY) || '');
    } catch (e) {
      console.error(e);
    }
    return tokens?.id_token ?? null;
  };

  public persistTokens = (tokens: AuthTokens) => {
    const expire_date = new Date().getTime() + tokens.expires_in * 1000;
    localStorage.setItem(
      TOKEN_STORAGE_KEY,
      JSON.stringify({
        ...tokens,
        expire_date,
      })
    );
  };

  public getTokens = (): AuthTokens | null => {
    let tokens: AuthTokens | null = null;
    try {
      tokens = JSON.parse(localStorage.getItem(TOKEN_STORAGE_KEY) || '');
    } catch (e) {
      console.error(e);
    }
    return tokens;
  };

  private refreshTokenErrorInterceptor = (axiosError: AxiosError) => {
    if (
      axiosError.response?.status === 401 ||
      axiosError.response?.status === 403
    ) {
      return this.refreshToken()
        .then((authInfo) => {
          this.persistTokens(authInfo);
          axiosError.config.headers = {
            ...axiosError.config.headers,
            Authorization: `Bearer ${authInfo.id_token}`,
          };
          return axios.request(axiosError.config);
        })
        .catch(() => {
          this.loginStatus.emit(AuthStatus.UNAUTHORIZED, true);
        });
    }
    return Promise.reject(axiosError.response?.data);
  };

  public login = (username: string, password: string) => {
    const params = new URLSearchParams({
      client_id: this.config.griffonClientId,
      client_secret: this.config.griffonClientSecret,
      grant_type: 'password',
      username,
      password,
    });
    return axios
      .post<AuthTokens>(`${this.config.griffonApiRoot}/oauth/token`, params)
      .then((res) => res.data);
  };

  public refreshToken = () => {
    const params = new URLSearchParams();
    params.append('client_id', this.config.griffonClientId);
    params.append('client_secret', this.config.griffonClientSecret);
    params.append('refresh_token', this.getTokens()?.refresh_token || '');
    params.append('grant_type', 'refresh_token');

    return axios
      .post<AuthTokens>(`${this.config.griffonApiRoot}/oauth/token`, params)
      .then((res) => res.data);
  };

  public sendOtp = (sid: string, code: string) => {
    return axios
      .post<SendOtpResponse>(
        `${this.config.griffonApiRoot}/oauth/signup/verify`,
        {
          code,
        },
        {
          params: {
            sid,
          },
        }
      )
      .then((res) => res.data);
  };

  public reSendOtp = (sid: string) => {
    return axios
      .post<SendOtpResponse>(
        `${this.config.griffonApiRoot}/oauth/signup/resend`,
        null,
        {
          params: {
            sid,
          },
        }
      )
      .then((res) => res.data);
  };

  public signUp = (username: string) => {
    const params = new URLSearchParams();
    params.append('client_id', this.config.griffonClientId);
    params.append('client_secret', this.config.griffonClientSecret);
    params.append('username', username);
    return axios
      .post<SignUpResponse>(
        `${this.config.griffonApiRoot}/oauth/signup`,
        params
      )
      .then((res) => res.data);
  };

  public register = (sid: string, password: string) => {
    return axios
      .post<RegisterResponse>(
        `${this.config.griffonApiRoot}/oauth/register`,
        {
          password,
        },
        {
          params: {
            sid,
          },
        }
      )
      .then((res) => res.data);
  };

  public getProfile = () => {
    return this.httpClient
      .get<Profile>(`${this.config.griffonApiRoot}/oauth/profile`, {
        headers: {
          Authorization: `Bearer ${this.getIdToken()}`,
        },
      })
      .then((res) => res.data);
  };

  public updateProfile = (data: ProfileUpdateDTO) => {
    return this.httpClient
      .put<Profile>(`${this.config.griffonApiRoot}/oauth/profile`, data, {
        headers: {
          Authorization: `Bearer ${this.getIdToken()}`,
        },
      })
      .then((res) => res.data);
  };

  public sendRestorePassword = (username: string) => {
    return axios
      .post<SignUpResponse>(
        `${this.config.griffonApiRoot}/oauth/password/reset`,
        {
          client_id: this.config.griffonClientId,
          username,
        }
      )
      .then((res) => res.data);
  };

  public verifyRestoreSecret = (sid: string, code: string) => {
    return axios
      .post<SendOtpResponse>(
        `${this.config.griffonApiRoot}/oauth/password/reset/verify`,
        {},
        {
          params: {
            sid,
            code,
          },
        }
      )
      .then((res) => res.data);
  };

  public setResetPassword = (sid: string, new_password: string) => {
    return axios
      .put(
        `${this.config.griffonApiRoot}/oauth/password/reset`,
        { new_password },
        {
          params: {
            sid,
          },
        }
      )
      .then((res) => res.data);
  };

  public logout = () => {
    localStorage.removeItem(PROFILE_STORAGE_KEY);
    localStorage.removeItem(TOKEN_STORAGE_KEY);
  };
}

export const authService = AuthService.createInstance({
  griffonApiRoot: environment.griffon.apiUrl,
  griffonClientId: environment.griffon.clientId,
  griffonClientSecret: environment.griffon.clientSecretId,
});

export const httpClient = authService.createHttpClient();
