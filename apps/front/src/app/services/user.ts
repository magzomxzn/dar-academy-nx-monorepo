import { LoginDTO, User } from '@dar-academy/domain';
import axios from 'axios';

export const login = (data: LoginDTO) => {
  return axios.post('/api/user/login', data).then((r) => r.data);
};
