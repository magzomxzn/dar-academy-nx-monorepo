import { LoginDTO } from '@dar-academy/domain';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { loginRequest } from '../../redux/async/user';
import { RootState, useAppDispatch } from '../../redux/store';

import styles from './LoginForm.module.scss';

const initialValue = {
  username: '',
  password: '',
};

const LoginForm: React.FC = () => {
  const dispatch = useAppDispatch();
  const { loginError, user, loginInProgress } = useSelector(
    (state: RootState) => state.user
  );
  const [formState, setFormState] = useState<LoginDTO>(initialValue);

  const handleChange = (field: string) => {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      setFormState((s) => ({
        ...s,
        [field]: e.target.value,
      }));
    };
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    console.log(formState);
    dispatch(loginRequest(formState));
  };

  return user ? (
    <div>{JSON.stringify(user)}</div>
  ) : (
    <form className={styles.form} noValidate onSubmit={handleSubmit}>
      {loginError && <div className={styles.error}>{loginError}</div>}
      <div className={styles.control}>
        <label className={styles.label}>Username</label>
        <input
          className={styles.input}
          type="text"
          value={formState.username}
          onChange={handleChange('username')}
        />
      </div>
      <div className={styles.control}>
        <label className={styles.label}>Password</label>
        <input
          className={styles.input}
          type="password"
          value={formState.password}
          onChange={handleChange('password')}
        />
      </div>
      <button
        className={styles.button}
        type="submit"
        disabled={loginInProgress}
      >
        {loginInProgress ? 'Loading' : 'Log in'}
      </button>
    </form>
  );
};

export default LoginForm;
