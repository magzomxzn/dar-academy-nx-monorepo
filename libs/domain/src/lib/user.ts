export interface User {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
}

export interface UserDTO {
  firstName: string;
  lastName: string;
  username: string;
}

export interface LoginDTO {
  username: string;
  password: string;
}
